var fs         = require('fs');
var pem        = require('pem');
var path       = require('path');
var uuid       = require('node-uuid');
var Handlebars = require('handlebars');

var secretKey = fs.readFileSync('privkey.pem', 'utf8');
var publicKey    = fs.readFileSync('pubkey.pem', 'utf8');

var source = fs.readFileSync('config-example.json', 'utf8');
var template  = Handlebars.compile(source);

var config = template({
 secret: secretKey.replace(/\r?\n|\r/g, '\\n'),
 public: publicKey.replace(/\r?\n|\r/g, '\\n')
})

fs.writeFileSync('config.json', config);

// pem.createCertificate({
//   days: 1,
//   selfSigned: true
// }, function(err, keys){
//
// });

// var secretKey = uuid.v4();
// var source = fs.readFileSync('config-example.json', 'utf8');
// var template = Handlebars.compile(source);
//
// var config = template({
//  secret: secretKey.replace(/\r?\n|\r/g, '\n'),
//  public: publicKey.replace(/\r?\n|\r/g, '\n')
// })
//
// fs.writeFileSync('config.json', config);
