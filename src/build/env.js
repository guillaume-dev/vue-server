var path = require('path')

module.exports = {
  port: 3000,
  index: path.resolve(__dirname, '../public/index.html'),
  assetsRoot: path.resolve(__dirname, '../public'),
  assetsSubDirectory: 'static',
  publicPath: '/'
}
