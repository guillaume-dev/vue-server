import mongoose from 'mongoose'
const Schema = mongoose.Schema

const UserSchema = new Schema({
    name: String,
    password: String,
    admin: Boolean,
    status: String
})

module.exports = mongoose.model('User', UserSchema)
