/**
* Load CSS here
**/
require('./styles/main.scss')

/**
* Load app
**/
require('./controllers/Router').init()
