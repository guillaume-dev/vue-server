export default {
  reverse (value) {
    return value.split('').reverse().join('')
  }
}
