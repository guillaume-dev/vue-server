module.exports = {
  '/': {
    name: 'home',
    component: require('../views/Home.vue'),
    subRoutes: {}
  },
  '/admin': {
    name: 'admin',
    component: require('../views/admin/Admin.vue'),
    auth: true,
    subRoutes: {
      '/projects': {
        name: 'projects',
        component: require('../views/admin/projects/Projects.vue'),
        subRoutes: {
          '/:id': {
            name: 'project',
            component: require('../views/admin/Admin.vue')
          }
        }
      }
    }
  },
  '/login': {
    name: 'login',
    component: require('../views/auth/Login.vue')
  },
  '/subscribe': {
    name: 'subscribe',
    component: require('../views/auth/Subscribe.vue')
  }
}
