const SERVER_URL  = 'http://localhost'
const SERVER_PORT = 3000
const API_URL     = 'http://localhost'
const API_PORT    = 3000
const SOCKET_URL  = window.location.host
const SOCKET_PORT = window.location.port

module.exports = {

  server: SERVER_URL + ':' + SERVER_PORT,

  api: API_URL + '/api',

  socket: SOCKET_URL + SOCKET_PORT,

  root: '/',

  getAPIRoute: function (name) {
    return API_URL + ':' + API_PORT + '/api/' + name
  }

}
