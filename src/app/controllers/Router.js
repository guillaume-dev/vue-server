import Vue from 'vue'
import Router from 'vue-router'
import Resource from 'vue-resource'

/* *********************************
****** CONFIG
**********************************/
import Network from '../config/Network'
import Routes from '../config/Routes'

/* *********************************
****** FILTERS
**********************************/
import Filters from '../helpers/Filters'

for (const key in Filters) {
  if (Filters.hasOwnProperty(key)) {
    Vue.filter(key, Filters[key])
  }
}

/* *********************************
****** AUTH
**********************************/
import Auth from '../controllers/state/Auth'

/* *********************************
****** COMPONENTS
**********************************/
import App from '../App.vue'

/* *********************************
****** RESOURCES
**********************************/

Vue.use(Resource)
Vue.http.options.root = Network.root
Vue.http.headers.common['Authorization'] = Auth.getAuthHeader()

/* *********************************
****** ROUTER
**********************************/

Vue.use(Router)
const router = new Router({
  hashbang: false,
  history: true,
  transitionOnLoad: true
})
router.mode = 'html5'

router.map(Routes)

router.beforeEach(function (transition) {
  if (transition.to.auth === true) {
    Auth.checkAuth().then((res) => {
      if (res === true) {
        transition.next()
        return
      } else {
        transition.redirect('/login')
      }
    }, (err) => {
      console.log('Error:', err)
      transition.redirect('/')
    })
  } else {
    transition.next()
  }
})

module.exports = {
  init () {
    router.start(App, '#app')
  }
}
