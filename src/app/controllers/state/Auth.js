import Request from 'superagent'
import Network from '../../config/Network'

class Auth {
  constructor() {
    this.user = {
      authenticated: false
    }
  }

  login (context, creds, callback) {
    context.$http.post(Network.getAPIRoute('authenticate'), creds).then((res) => {
      if (res.data.success === true) {
        this.user.authenticated = true
      } else {
        this.user.authenticated = false
      }

      if (callback) {
        callback(res.data, this.user.authenticated)
      }

    }, (err) => {
      console.log('Error:', err)
      context.error = err
    })
  }

  signup (context, creds, callback) {
    context.$http.post(SIGNUP_URL, creds, (data) => {
      if (data.success === true) {
        this.user.authenticated = true
      } else {
        this.user.authenticated = false
      }

      if (callback) {
        callback(this.user.authenticated)
        // router.go(redirect)
      }

    }, (err) => {
      context.error = err
    })
  }

  logout (callback) {
    // localStorage.removeItem('id_token')
    Request.get(Network.getAPIRoute('logout'))
            .end((err, res) => {
              if (err) {
                callback(err)
                return
              }

              let json = JSON.parse(res.text)
              if (json.success === true) {
                this.user.authenticated = false
              }
              if (callback !== undefined) {
                callback(json)
              }
            })
  }

  checkAuth () {
    return new Promise((resolve, reject) => {
      Request.get(Network.getAPIRoute('check'))
              .end((err, res) => {
                if (err) {
                  reject(err)
                  return
                }
                let json = JSON.parse(res.text)
                resolve(json.success)
              })
    })
    // const jwt = localStorage.getItem('id_token')
    // if (jwt !== undefined && jwt !== null) {
    //   this.user.authenticated = true
    // } else {
    //   this.user.authenticated = false
    // }
    // return this.user.authenticated
  }

  getAuthHeader() {
    return {
      'Authorization': 'Bearer ' + localStorage.getItem('id_token')
    }
  }
}

export default new Auth()
