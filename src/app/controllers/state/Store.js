import Vue from 'vue'
import Vuex from 'vuex'
import Auth from './Auth'

Vue.use(Vuex)

const state = {
  color: '#414b47',
  auth: Auth
}

const mutations = {
  CHANGE_COLOR (state, hex) {
    state.color = hex
  },

  SET_USER (state, user) {
    state.auth.user = user
  },

  LOGIN (state, component, credentials, callback) {
    state.auth.login(component, credentials, callback)
  },

  LOGOUT (state, callback) {
    state.auth.logout(callback)
  }
}

export default new Vuex.Store({
  state,
  mutations
})
