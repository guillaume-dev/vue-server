export const setColor = ({ dispatch }, hex) => {
  dispatch('CHANGE_COLOR', hex)
}

export const setUser = ({ dispatch }, user) => {
  dispatch('SET_USER', user)
}

export const login = ({ dispatch }, component, credentials, callback) => {
  dispatch('LOGIN', component, credentials, callback)
}

export const logout = ({ dispatch }, callback) => {
  dispatch('LOGOUT', callback)
}
