module.exports = {
  root: true,
  parserOptions: {
    sourceType: 'module'
  },
  extends: 'standard',
  plugins: [
    'html'
  ],
  // add your custom rules here
  'rules': {
    'no-multi-spaces': 0,
    'no-trailing-spaces': 0,
    'no-multiple-empty-lines': 0,
    'no-useless-constructor': 1,
    'no-unused-vars': 1,
    'no-undef': 1,
    'padded-blocks': 0,
    'space-before-function-paren': 0,
    'space-in-parens': 0,
    'no-unreachable': 1,
    'no-irregular-whitespace': 0,
    'arrow-parens': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
