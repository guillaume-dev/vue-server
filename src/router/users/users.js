import User from '../../models/user'

export const get = function (req, res) {
  User.find({}, function (err, users) {
    res.json({
      success: true,
      message: 'List of users',
      users: users
    })
  })
}
