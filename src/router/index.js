import express from 'express'
import jwt from 'jsonwebtoken'
import User from '../models/user'
import * as UserRoutes from './users/users'
import * as AuthRoutes from './api/auth'

export default class Router extends express.Router {
  constructor() {
    super()

    // this.use()

    this.route('/setup')
        .get(function(req, res) {
          User.remove({}).exec()
          const user = new User({
            name: 'guillaumerxl',
            password: 'password',
            admin: true,
            status: 'active'
          })

          // save the sample user
          user.save(function(err) {
            if (err) throw err

            res.json({
              success: true,
              message: 'User added successfully',
              user: user
            })
          })
        })

    this.route('/authenticate')
        .post(AuthRoutes.authenticate)

    this.route('/check')
        .get(AuthRoutes.checkAuth)

    this.route('/logout')
        .get(AuthRoutes.revokeAuth)

    this.route('/users')
        .all(AuthRoutes.verifyToken)
        .get(UserRoutes.get)

  }
}
