import fs from 'fs'
import express from 'express'
import jwt from 'jsonwebtoken'
import Cookies from 'cookies'
import User from '../../models/user'
import config from '../../../config.json'

// console.log(config.auth.secret)
config.auth.secret = fs.readFileSync('privkey.pem', 'utf8')
config.auth.public = fs.readFileSync('pubkey.pem', 'utf8')
// console.log(config)

export const verifyToken = function(req, res, next) {
  // check header or url parameters or post parameters for token
  // const token = req.body.token || req.query.token || req.headers['x-access-token']
  const token = new Cookies(req, res).get('access_token')
  // decode token
  if (token !== undefined) {
    // verifies secret and checks exp
    jwt.verify(token, config.auth.public, function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' })
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded
        next()
      }
    })

  } else {
    // if there is no token
    // return an error
    return res.status(403).send({
        success: false,
        message: 'No token provided.'
    })
  }
}

export const authenticate = function (req, res) {
  // HANDLE ERRORS HERE
  User.findOne({
    name: req.body.name
  }, function(err, user) {

    if (err) throw err

    if (user === undefined) {
      res.json({
        success: false,
        message: 'Authentication failed. User not found.'
      })
    } else if (user) {
      // check if password matches
      if (user.password !== req.body.password) {
        res.json({
          success: false,
          message: 'Authentication failed. Wrong password.'
        })
      } else {
        // if user is found and password is right
        // create a token
        var token = jwt.sign(user, config.auth.secret, {
          algorithm: 'RS256'
        })

        // return the information including token as JSON
        new Cookies(req, res).set('access_token', token, {
          httpOnly: true,
          // secure: true
        })

        res.json({
          success: true,
          message: 'Token is set'
        })
      }

    }

  })
}

export const checkAuth = function(req, res, next) {

  const token = new Cookies(req, res).get('access_token')
  // decode token
  if (token !== undefined) {
    // verifies secret and checks exp
    jwt.verify(token, config.auth.public, function(err, decoded) {
      if (err) {
        return res.json({
          success: false,
          message: 'Failed to authenticate token.'
        })
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded
        return res.status(200).send({
            success: true,
            message: 'User is authenticated.'
        })
      }
    })

  } else {
    return res.status(200).send({
        success: false,
        message: 'User is not authenticated.'
    })
  }
}

export const revokeAuth = function (req, res) {
  new Cookies(req, res).set('access_token', null, {
    httpOnly: true,
    overwrite: true,
    expires: new Date(70)
  })

  res.json({
    success: true,
    message: 'Token is removed'
  })
}

function base64Decode(base64str) {
  return new Buffer(base64str, 'base64');
}

function base64Encode(bitmap) {
  return new Buffer(bitmap).toString('base64');
}
