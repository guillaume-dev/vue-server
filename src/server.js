import cors from 'cors'
import path from 'path'
import express from 'express'
import mongoose from 'mongoose'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import webpack from 'webpack'
import fallback from 'express-history-api-fallback'
import webpackMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from './build/webpack.config.js'
import Router from './router/index'
import env from './build/env'
import mongo from './utils/mongo'
const config = require('../config.json')

mongoose.connect(mongo.getURL(
  config.database.url,
  config.database.name
))

const app = express()

const port = process.env.PORT || env.port
const developing = process.env.ENV === undefined || process.env.ENV !== 'production'

let base = path.resolve(__dirname, '../public')

if (developing === true) {
  base = path.resolve(__dirname, './app')
  const compiler = webpack(webpackConfig)
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.publicPath,
    contentBase: './app',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  })

  app.use(middleware)
  app.use(webpackHotMiddleware(compiler))
}

const root = base
const staticPath = path.posix.join(env.publicPath, env.assetsSubDirectory)

app.set('secret', config.auth.secret)
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))

//!\\ MUST BE INSERTED BEFORE THE FALLBACK FOR HTML5 HISTORY
app.use('/api', new Router())

app.use(staticPath, express.static(root))
app.use(fallback('index.html', { root }))


app.listen(port, function (err) {
  if (err) {
    console.log(err)
    return
  }
  console.log('Listening at http://localhost:' + port + '\n')
})
