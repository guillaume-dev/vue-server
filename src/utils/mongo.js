module.exports = {
  getURL: function (url, name, port = 27017, user = null, password = null) {
    let auth = ''
    if (user !== null && password !== null) {
      auth = user + ':' + password + '@'
    }
    // mongoose.connect('mongodb://localhost:27017/backy')
    return 'mongodb://' + auth + url + ':' + port + '/' + name
  }
}
